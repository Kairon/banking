<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $type
 * @property double $amount
 * @property int $client_id
 * @property int $deposit_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Client $client
 * @property Deposit $deposit
 */
class Transaction extends \yii\db\ActiveRecord
{
    const TYPE_ADD = 0;
    const TYPE_WITHDRAW = 1;

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getTypeName(){
        return ArrayHelper::getValue(self::getTypesArray(), $this->type);
    }

    public static function getTypesArray()
    {
        return [
            self::TYPE_ADD => 'Add',
            self::TYPE_WITHDRAW => 'Withdraw',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'client_id', 'deposit_id', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['deposit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deposit::className(), 'targetAttribute' => ['deposit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'amount' => 'Amount',
            'client_id' => 'Client',
            'deposit_id' => 'Deposit #',
            'created_at' => 'Date',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeposit()
    {
        return $this->hasOne(Deposit::className(), ['id' => 'deposit_id']);
    }

}
