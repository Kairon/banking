<?php

namespace app\models;

use nsept\birthdaypicker\BirthdayValidator;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $name
 * @property string $second_name
 * @property int $gender
 * @property int $birthday
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Deposit[] $deposits
 */
class Client extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gender', 'birthday', 'created_at', 'updated_at'], 'integer'],
            [['name', 'second_name'], 'string', 'max' => 255],
            //[['birthday'], 'date']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'second_name' => 'Second Name',
            'gender' => 'Gender',
            'birthday' => 'Birthday',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeposits()
    {
        return $this->hasMany(Deposit::className(), ['client_id' => 'id']);
    }

    public function getFullName(){
        return $this->name . " " . $this->second_name;
    }
}
