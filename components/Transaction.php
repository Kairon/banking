<?php
/**
 * Created by PhpStorm.
 * User: ivakhnov
 * Date: 10/10/18
 * Time: 11:12 PM
 */
namespace app\components;

use app\models\Deposit;

class Transaction
{
    const MIN_DAYS_IN_MONTH = 2370543; //28 days

    public function addTransactions($type){
        if ($type)
            $this->withdrawAmount();
        else
            $this->setAmount($this->getDeposits());
    }

    protected function getDeposits(){
        return Deposit::find()
            ->where(['<=', 'updated_at', time() - self::MIN_DAYS_IN_MONTH])
            ->all();
    }

    protected function setAmount(array $models){
        foreach ($models as $model){
            $dep_day = $this->getDay($model->updated_at);
            $day_in_month = cal_days_in_month(CAL_GREGORIAN, $this->getMonth(), $this->getYear());
            $percent = ($model->amount * $model->percent / 100);

            if ($day_in_month <= $dep_day){
                $model->amount += $percent;
                $model->save();
                (new \app\models\Transaction([
                    'type' => \app\models\Transaction::TYPE_ADD,
                    'amount' => $percent,
                    'client_id' => $model->client_id,
                    'deposit_id' => $model->id,
                ]))->save(true);
            }elseif ($dep_day == $this->getDay()){
                $model->amount += $percent;
                $model->save();
                (new \app\models\Transaction([
                    'type' => \app\models\Transaction::TYPE_ADD,
                    'amount' => $percent,
                    'client_id' => $model->client_id,
                    'deposit_id' => $model->id,
                ]))->save(true);
            }
        }
    }

    protected function withdrawAmount(){
        $models = Deposit::find()->all();
        foreach ($models as $model){
            $withdraw = $this->getWithdraw($model->amount);
            $model->amount -= $withdraw;
            $model->save(false);

            (new \app\models\Transaction([
                'type' => \app\models\Transaction::TYPE_WITHDRAW,
                'amount' => $withdraw,
                'client_id' => $model->client_id,
                'deposit_id' => $model->id,
            ]))->save(true);
        }
    }

    private function getMonth(){
        return date('m', time());
    }

    private function getYear(){
        return date('Y', time());
    }

    private function getDay($timestamp = null){
        return date('d', $timestamp ? $timestamp : time());
    }

    private function getWithdraw($amount){
        $withdrawAmount = 0;
        switch (true) {
            case ($amount < 1000) :
                $withdrawAmount = $amount * 5 / 100;
                if($withdrawAmount < 50)
                    $withdrawAmount = 50;
                break;
            case ($amount >= 1000 && $amount < 10000):
                $withdrawAmount = $amount * 6 / 100;
                break;
            case ($amount >= 10000):
                $withdrawAmount = $amount * 7 / 100;
                if($withdrawAmount > 5000)
                    $withdrawAmount = 5000;
                break;
        }

        return $withdrawAmount;
    }
}