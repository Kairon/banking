<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transaction`.
 */
class m181010_191726_create_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'amount' => $this->float(),
            'client_id' => $this->integer(),
            'deposit_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-transaction-client_id',
            'transaction',
            'client_id',
            'client',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-transaction-deposit_id',
            'transaction',
            'deposit_id',
            'deposit',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('transaction');
    }
}
