<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deposit`.
 */
class m181010_124935_create_deposit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('deposit', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'percent' => $this->float(2),
            'amount' => $this->float(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // add foreign key for table `client`
        $this->addForeignKey(
            'fk-deposit-client_id',
            'deposit',
            'client_id',
            'client',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('deposit');
    }
}
