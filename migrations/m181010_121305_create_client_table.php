<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client`.
 */
class m181010_121305_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'second_name' => $this->string(),
            'gender' => $this->integer(2),
            'birthday' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('client');
    }
}
